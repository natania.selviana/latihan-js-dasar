//this
// var a = 10;
// console.log(window.a);

//cara1 - function declaration
// function halo(){
//     console.log(this);
//     console.log('halo');
// }
// this.halo();
// this mengembalikan object global

//cara2 - object literal
// var obj = {a : 10, nama : 'natania'};
// obj.halo = function(){
//     console.log(this);
//     console.log('halo');
// }
// obj.halo();
//this mengembalikan object yang bersangkutan

//cara3 - constructor
function Halo(){
    console.log(this);
    console.log('halo');
}
var obj1 = new Halo();
var obj2 = new Halo();
//this mengembalikan object yang baru di buat














