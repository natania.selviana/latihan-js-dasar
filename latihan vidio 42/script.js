// membuat object
// object literal
var mhs1 = {
    nama : 'Natania Selviana',
    nrp : '043040023',
    email : 'selviananatania6@gmail.com',
    jurusan : 'Teknik Informatika'
}

var mhs2 = {
    nama : 'Sandhika Galih',
    nrp : '0430400007',
    email : 'sandhikagalih@unpas.ac.id',
    jurusan : 'Teknik Industri'
}

// Function Declaration
function buatObjectMahasiswa(nama, nrp, email, jurusan){
    var mhs = {}
    mhs.nama = nama;
    mhs.nrp = nrp;
    mhs.email = email;
    mhs.jurusan = jurusan;
    return mhs;
}

var mhs3 = buatObjectMahasiswa('Natania', '023040123', 'selviananaania@gmail.com', 'Teknik Pangan')


//Constructor
function Mahasiswa(nama, nrp, email, jurusan){
    //var this = {};
    this.nama = nama;
    this.nrp = nrp;
    this.email = email;
    this.jurusan = jurusan;
    // return this;
}

var mhs4 = new Mahasiswa('Erik', '01239094878', 'erik01@gmail.com', 'Teknik Mesin');

